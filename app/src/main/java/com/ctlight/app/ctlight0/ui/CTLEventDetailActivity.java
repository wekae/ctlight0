package com.ctlight.app.ctlight0.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.ctlight.app.ctlight0.MainActivity;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.utils.CTLightBaseActivity;

/**
 * An activity representing a single CTLEvent detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 */
public class CTLEventDetailActivity extends CTLightBaseActivity {

    private final String LOG_TAG = CTLEventDetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ctlevent_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putString(CTLEventDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(CTLEventDetailFragment.ARG_ITEM_ID));

            CTLEventDetailFragment fragment = new CTLEventDetailFragment();
            arguments.putAll(getIntent().getExtras());
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().replace(R.id.ctlevent_detail_container, fragment, CTLEventDetailFragment.FRAGMENT_TAG).commit();
        } else {
            CTLEventDetailFragment ctlEventDetailFragment = (CTLEventDetailFragment) getSupportFragmentManager().findFragmentByTag(CTLEventDetailFragment.FRAGMENT_TAG);
        }
//        if (savedInstanceState == null) {
//            // Create the detail fragment and add it to the activity
//            // using a fragment transaction.
//            CTLEventDetailFragment fragment = new CTLEventDetailFragment();
//            fragment.setArguments(arguments);
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.ctlevent_detail_container, fragment)
//                    .commit();
//        }



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, CTLEventListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
