package com.ctlight.app.ctlight0.ui;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.adapters.CTLEventAdapter;
import com.ctlight.app.ctlight0.background.AsyncResponse;
import com.ctlight.app.ctlight0.background.LoadCTLEvents;
import com.ctlight.app.ctlight0.providers.CTLightEventsProvider;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.viewmodels.CTLEventsViewModel;
import com.ctlight.app.ctlight0.utils.AnimationUtils;
import com.ctlight.app.ctlight0.utils.HttpUtils;
import com.ctlight.app.ctlight0.utils.NotificationUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class CTLEventListFragment extends Fragment implements AsyncResponse<Observable<JSONArrayResponse<CTLEvent>>> {

    private static final String LOG_TAG = CTLEventListFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = CTLEventListFragment.class.getSimpleName();
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private Context context;
    private Activity activity;

    private RecyclerView mRecyclerView;

    static CTLEventListFragment fragment;

    private CTLEventsViewModel ctlEventsViewModel;


    private LinearLayout lytLoading;




    public static final String KEY_ITEM = "unique_key";
    public static final String KEY_INDEX = "index_key";
    private String mTime;


    public CTLEventListFragment() {
        // Required empty public constructor
    }

    public static CTLEventListFragment newInstance() {
        if(fragment==null){
            fragment = new CTLEventListFragment();
        }
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            // Restore last state
            mTime = savedInstanceState.getString("time_key");
        } else {
            mTime = "" + Calendar.getInstance().getTimeInMillis();
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Log.d(LOG_TAG, "Creating Fragment");

        if (getArguments() != null) {

        }

        if (activity.findViewById(R.id.ctlevent_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }


        mRecyclerView = activity.findViewById(R.id.ctlevent_list);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ctlevent_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Log.d(LOG_TAG, "Creating RecyclerView");
        assert mRecyclerView != null;
        setupRecyclerView();
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        context = ctx;
        activity = (Activity)ctx;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("time_key", mTime);
    }


    private void setupRecyclerView() {
//        LoadCTLEvents loadCTLEvents = new LoadCTLEvents(context);
//        loadCTLEvents.delegate = this;

        //Implement asynctask
//        loadCTLEvents.execute();
        final CTLEventAdapter ctlEventAdapter = new CTLEventAdapter(activity, mTwoPane);


        //Run if network connection is established
        if(HttpUtils.connectionExists(activity)){
            ctlEventsViewModel = ViewModelProviders.of(this).get(CTLEventsViewModel.class);
            ctlEventsViewModel.getCtlEvents().observe(this, new android.arch.lifecycle.Observer<JSONArrayResponse<CTLEvent>>() {
                @Override
                public void onChanged(@Nullable JSONArrayResponse<CTLEvent> ctlEventJSONArrayResponse) {

                    ArrayList<CTLEvent> ctlEvents = new ArrayList<>();
                    ctlEvents = ctlEventJSONArrayResponse.getData();
//            Toast.makeText(context, "Finishing: "+ctlEvents.size(), Toast.LENGTH_LONG).show();
//            for(CTLEvent event: ctlEvents){
//                Log.d(LOG_TAG, "EVENT: "+event.getTitle());
//            }

                    ctlEventAdapter.setValues(ctlEvents);

                    mRecyclerView.setAdapter(ctlEventAdapter);
                    mRecyclerView.setVisibility(View.VISIBLE);

                    lytLoading = activity.findViewById(R.id.lyt_loading);
                    lytLoading.setVisibility(View.GONE);

                    ctlEventAdapter.notifyDataSetChanged();
                }
            });

        }else{
            NotificationUtils.showDialogBox(activity, "No Network", "Check your internet connection");
        }

    }


    @Override
    public void processFinish(Observable<JSONArrayResponse<CTLEvent>> responseObservable){
        responseObservable.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<JSONArrayResponse<CTLEvent>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(JSONArrayResponse<CTLEvent> value) {
                    ArrayList<CTLEvent> ctlEvents = new ArrayList<>();
                    ctlEvents = value.getData();
//                    Toast.makeText(context, "Finishing: "+ctlEvents.size(), Toast.LENGTH_LONG).show();

//                    mRecyclerView.setAdapter(new CTLEventAdapter(activity, ctlEvents, mTwoPane));
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });

    }





}
