package com.ctlight.app.ctlight0.providers.mock;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;

import java.util.ArrayList;

public class CTLightEventsProvider {

    public static ArrayList<CTLEvent> allEvents(){

        ArrayList<CTLEvent> ctlEvents = new ArrayList<>();

        CTLEvent ctlEvent = new CTLEvent();
        ctlEvent.setId(24);
        ctlEvent.setTitle("Aliquam hendrerit");
        ctlEvent.setDate("2018-02-11");
        ctlEvent.setVenue("Duis tristique tortor");
        ctlEvent.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor vehicula quam vestibulum maximus. Donec aliquet malesuada sem in tincidunt. Maecenas sollicitudin varius ipsum placerat euismod. Donec sit amet feugiat ipsum. Aliquam hendrerit porttitor tellus, non lobortis risus tempor a. Duis tristique tortor a elit pretium, id mollis massa tristique. Vestibulum luctus porta mauris, eget porttitor enim porttitor quis. Duis pharetra consequat egestas.");
        ctlEvent.setAdditionalInfo("Maecenas suscipit felis eu nisl suscipit viverra. Sed pulvinar ante in diam fringilla dignissim. Proin ullamcorper pulvinar risus at eleifend. Quisque ullamcorper orci sit amet orci bibendum egestas nec quis libero. Cras lobortis mauris ut dui egestas, sed aliquam lorem lobortis. Sed porta porta tellus, ac rhoncus nunc pulvinar nec. Nulla ultricies erat sit amet lacus commodo finibus. Maecenas faucibus tempor tempor. Duis nec nunc lacinia, ultricies diam vitae, lacinia nibh.");
        ctlEvent.setImage(true);
        ctlEvent.setToken("6b0e19903aac5098936594f6cec834ff0e2e1e131e418b6f51714e0beb436c19");


        CTLEvent ctlEvent1 = ctlEvent;
        CTLEvent ctlEvent2 = ctlEvent;
        CTLEvent ctlEvent3 = ctlEvent;
        CTLEvent ctlEvent4 = ctlEvent;
        CTLEvent ctlEvent5 = ctlEvent;


        ctlEvents.add(ctlEvent);
        ctlEvents.add(ctlEvent1);
        ctlEvents.add(ctlEvent2);
        ctlEvents.add(ctlEvent3);
        ctlEvents.add(ctlEvent4);
        ctlEvents.add(ctlEvent5);

        return ctlEvents;

    }


    public static CTLEvent getEvent(String id){
        CTLEvent ctlEvent = new CTLEvent();
        ctlEvent.setId(24);
        ctlEvent.setTitle("Aliquam hendrerit empor vehicula quam vestibulum");
        ctlEvent.setDate("2018-02-11");
        ctlEvent.setVenue("Duis tristique tortor");
        ctlEvent.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor vehicula quam vestibulum maximus. Donec aliquet malesuada sem in tincidunt. Maecenas sollicitudin varius ipsum placerat euismod. Donec sit amet feugiat ipsum. Aliquam hendrerit porttitor tellus, non lobortis risus tempor a. Duis tristique tortor a elit pretium, id mollis massa tristique. Vestibulum luctus porta mauris, eget porttitor enim porttitor quis. Duis pharetra consequat egestas.");
        ctlEvent.setAdditionalInfo("Maecenas suscipit felis eu nisl suscipit viverra. Sed pulvinar ante in diam fringilla dignissim. Proin ullamcorper pulvinar risus at eleifend. Quisque ullamcorper orci sit amet orci bibendum egestas nec quis libero. Cras lobortis mauris ut dui egestas, sed aliquam lorem lobortis. Sed porta porta tellus, ac rhoncus nunc pulvinar nec. Nulla ultricies erat sit amet lacus commodo finibus. Maecenas faucibus tempor tempor. Duis nec nunc lacinia, ultricies diam vitae, lacinia nibh.");
        ctlEvent.setImage(true);
        ctlEvent.setToken("6b0e19903aac5098936594f6cec834ff0e2e1e131e418b6f51714e0beb436c19");

        return ctlEvent;
    }
}
