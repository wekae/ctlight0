package com.ctlight.app.ctlight0.providers.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.widget.Toast;

import com.ctlight.app.ctlight0.providers.CTLightEventsProvider;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;

import java.util.ArrayList;
import java.util.Observable;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CTLEventsRepository {
    private LiveData<io.reactivex.Observable<JSONArrayResponse<CTLEvent>>>  mCTLEvents;
    Application mApplication;
    public CTLEventsRepository(Application application){
        mApplication = application;
    }

    public LiveData<JSONArrayResponse<CTLEvent>> getAllEvents(){
        final MutableLiveData<JSONArrayResponse<CTLEvent>> data = new MutableLiveData<>();
        CTLightEventsProvider.allEvents().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JSONArrayResponse<CTLEvent>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(JSONArrayResponse<CTLEvent> value) {
                        data.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
//                        Toast.makeText(mApplication, "DONE", Toast.LENGTH_SHORT).show();

                    }
                });

        return data;
    }

    public LiveData<JSONObjectResponse<CTLEvent>> getEvent(String id){
        final MutableLiveData<JSONObjectResponse<CTLEvent>> data = new MutableLiveData<>();
        CTLightEventsProvider.getEvent(id).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<JSONObjectResponse<CTLEvent>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(JSONObjectResponse<CTLEvent> value) {
                    data.setValue(value);
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {
//                    Toast.makeText(mApplication, "DONE", Toast.LENGTH_SHORT).show();

                }
        });

        return data;
    }


}
