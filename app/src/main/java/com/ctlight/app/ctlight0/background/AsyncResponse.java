package com.ctlight.app.ctlight0.background;

public interface AsyncResponse<T> {
    void processFinish(T response);
}
