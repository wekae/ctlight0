package com.ctlight.app.ctlight0.providers.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.repository.CTLEventsRepository;
import com.ctlight.app.ctlight0.providers.repository.CTLVersesRepository;

public class CTLVersesViewModel extends AndroidViewModel {
    private CTLVersesRepository ctlVersesRepository;


    private LiveData<JSONArrayResponse<CTLVerse>> ctlVerses;

    public CTLVersesViewModel(Application application){
        super(application);

        ctlVersesRepository = new CTLVersesRepository(application);
        ctlVerses = ctlVersesRepository.getAllVerses();
    }


    public LiveData<JSONArrayResponse<CTLVerse>> getCtlVerses() {
        return ctlVerses;
    }
}
