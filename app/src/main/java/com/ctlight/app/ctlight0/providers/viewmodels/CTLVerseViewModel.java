package com.ctlight.app.ctlight0.providers.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;
import com.ctlight.app.ctlight0.providers.repository.CTLEventsRepository;
import com.ctlight.app.ctlight0.providers.repository.CTLVersesRepository;

public class CTLVerseViewModel extends AndroidViewModel {
    private CTLVersesRepository ctlVersesRepository;


    private LiveData<JSONObjectResponse<CTLVerse>> ctlVerse;
    private String mId;

    public CTLVerseViewModel(Application application){
        super(application);

        ctlVersesRepository = new CTLVersesRepository(application);
    }


    public LiveData<JSONObjectResponse<CTLVerse>> getCtlVerse(String id) {
        if(ctlVerse!=null && mId!=null && mId.equals(id)) {
            return ctlVerse;
        }else{
            mId = id;
            ctlVerse = ctlVersesRepository.getVerse(id);
            return ctlVerse;
        }
    }

    public LiveData<JSONObjectResponse<CTLVerse>> getCtlTodaysVerse() {
        if(ctlVerse!=null) {
            return ctlVerse;
        }else{
            ctlVerse = ctlVersesRepository.getTodaysVerse();
            return ctlVerse;
        }
    }
}
