package com.ctlight.app.ctlight0.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.ui.CTLEventDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLEventDetailFragment;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailFragment;
import com.ctlight.app.ctlight0.utils.AnimationUtils;

import java.util.List;

public class CTLVerseAdapter extends RecyclerView.Adapter<CTLVerseAdapter.ViewHolder> {


    private final FragmentActivity mParentActivity;
    private Context context;
    private List<CTLVerse> mValues;
    private final boolean mTwoPane;

    private final LayoutInflater mInflater;

    private static final String LOG_TAG = CTLVerseAdapter.class.getSimpleName();



    public CTLVerseAdapter(Activity parent, boolean twoPane) {
        mParentActivity = (FragmentActivity)parent;
        context = parent;
        mTwoPane = twoPane;

        mInflater = LayoutInflater.from(context);
    }


    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            CTLVerse item = (CTLVerse) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(CTLVerseDetailFragment.ARG_ITEM_ID, item.getId().toString());
                CTLVerseDetailFragment fragment = new CTLVerseDetailFragment();
                fragment.setArguments(arguments);

                // Call another fragment
                FragmentTransaction fragmentTransaction = mParentActivity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.ctlevent_detail_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, CTLVerseDetailActivity.class);
                intent.putExtra(CTLVerseDetailFragment.ARG_ITEM_ID, item.getId().toString());

                context.startActivity(intent);
            }
        }
    };

    @Override
    public CTLVerseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ctlverse_list_content, parent, false);
        return new CTLVerseAdapter.ViewHolder(view);
    }

    public void setValues(List<CTLVerse> values){
        mValues = values;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final CTLVerseAdapter.ViewHolder holder, int position) {
        holder.mTxtTitle.setText(mValues.get(position).getTitle());
        holder.mTxtVerse.setText(mValues.get(position).getVerse());
        holder.mTxtDate.setText(mValues.get(position).getDate());
        holder.mTxtDescription.setText(mValues.get(position).getDescription());
//        Glide.with(context).load(R.drawable.cl_light_logo).into(holder.mImgVerse);

        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtTitle;
        private TextView mTxtVerse;
        private TextView mTxtDate;
        private TextView mTxtDescription;
        private ImageView mImgVerse;

        ViewHolder(View view) {
            super(view);
            mTxtTitle = (TextView) view.findViewById(R.id.txt_card_title);
            mTxtVerse = (TextView) view.findViewById(R.id.txt_card_verse);
            mTxtDate = (TextView) view.findViewById(R.id.txt_card_date);
            mTxtDescription = (TextView) view.findViewById(R.id.txt_card_description);
            mImgVerse = (ImageView) view.findViewById(R.id.img_card_verse);

            view.setAnimation(AnimationUtils.fadeIn());
        }
    }
}