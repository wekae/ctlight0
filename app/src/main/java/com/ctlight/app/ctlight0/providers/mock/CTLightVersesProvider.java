package com.ctlight.app.ctlight0.providers.mock;

import com.ctlight.app.ctlight0.providers.models.CTLVerse;

import java.util.ArrayList;

public class CTLightVersesProvider {

    public static ArrayList<CTLVerse> allVerses(){
        ArrayList<CTLVerse> ctlVerses = new ArrayList<>();

        CTLVerse ctlVerse = new CTLVerse();
        ctlVerse.setId(8);
        ctlVerse.setTitle("Faisque fermentum, lectus eget");
        ctlVerse.setDate("2018-09-13");
        ctlVerse.setVerse("KIN 5:1-3");
        ctlVerse.setContents("1 When Hiram king of Tyre heard that Solomon had been anointed king to succeed his father David, he sent his envoys to Solomon, because he had always been on friendly terms with David.\n2 Solomon sent back this message to Hiram:\n3 “You know that because of the wars waged against my father David from all sides, he could not build a temple for the Name of the LORD his God until the LORD put his enemies under his feet.");
        ctlVerse.setDescription("Morbi iaculis justo eget lorem elementum, id sollicitudin magna tempus. Phasellus blandit egestas ex, ac ornare lorem congue sed.");
        ctlVerse.setEncouragement("Donec id risus justo. Vivamus feugiat orci vel nunc fringilla, in vulputate sapien convallis. Sed volutpat risus lorem, nec lobortis massa mattis eget");
        ctlVerse.setToken("d54240fc6c96b2724ec77a1f99731bab0e5a81e5e02e3aef5f954e2c4544a0a8");
        ctlVerse.setImage(true);


        CTLVerse ctlVerse1 = ctlVerse;
        CTLVerse ctlVerse2 = ctlVerse;
        CTLVerse ctlVerse3 = ctlVerse;
        CTLVerse ctlVerse4 = ctlVerse;
        CTLVerse ctlVerse5 = ctlVerse;
        CTLVerse ctlVerse6 = ctlVerse;
        CTLVerse ctlVerse7 = ctlVerse;
        CTLVerse ctlVerse8 = ctlVerse;
        CTLVerse ctlVerse9 = ctlVerse;
        CTLVerse ctlVerse10 = ctlVerse;
        CTLVerse ctlVerse11 = ctlVerse;

        ctlVerses.add(ctlVerse);
        ctlVerses.add(ctlVerse1);
        ctlVerses.add(ctlVerse2);
        ctlVerses.add(ctlVerse3);
        ctlVerses.add(ctlVerse4);
        ctlVerses.add(ctlVerse5);
        ctlVerses.add(ctlVerse6);
        ctlVerses.add(ctlVerse7);
        ctlVerses.add(ctlVerse8);
        ctlVerses.add(ctlVerse9);
        ctlVerses.add(ctlVerse10);
        ctlVerses.add(ctlVerse11);

        return ctlVerses;
    }


    public static CTLVerse getVerse(String id){
        CTLVerse ctlVerse = new CTLVerse();



        ctlVerse.setId(8);
        ctlVerse.setTitle("Faisque fermentum, lectus eget");
        ctlVerse.setDate("2018-09-13");
        ctlVerse.setVerse("KIN 5:1-3");
        ctlVerse.setContents("1 When Hiram king of Tyre heard that Solomon had been anointed king to succeed his father David, he sent his envoys to Solomon, because he had always been on friendly terms with David.\n2 Solomon sent back this message to Hiram:\n3 “You know that because of the wars waged against my father David from all sides, he could not build a temple for the Name of the LORD his God until the LORD put his enemies under his feet.");
        ctlVerse.setDescription("Morbi iaculis justo eget lorem elementum, id sollicitudin magna tempus. Phasellus blandit egestas ex, ac ornare lorem congue sed.");
        ctlVerse.setEncouragement("Donec id risus justo. Vivamus feugiat orci vel nunc fringilla, in vulputate sapien convallis. Sed volutpat risus lorem, nec lobortis massa mattis eget");
        ctlVerse.setToken("d54240fc6c96b2724ec77a1f99731bab0e5a81e5e02e3aef5f954e2c4544a0a8");
        ctlVerse.setImage(true);


        return ctlVerse;
    }

    public static CTLVerse getTodaysVerse(){
        CTLVerse ctlVerse = new CTLVerse();



        ctlVerse.setId(8);
        ctlVerse.setTitle("Today's Verse Faisque lectus eget");
        ctlVerse.setDate("2018-09-13");
        ctlVerse.setVerse("KIN 5:1-3");
        ctlVerse.setContents("1 When Hiram king of Tyre heard that Solomon had been anointed king to succeed his father David, he sent his envoys to Solomon, because he had always been on friendly terms with David.\n2 Solomon sent back this message to Hiram:\n3 “You know that because of the wars waged against my father David from all sides, he could not build a temple for the Name of the LORD his God until the LORD put his enemies under his feet.");
        ctlVerse.setDescription("Morbi blandit egestas ex, ac ornare lorem congue sed.");
        ctlVerse.setEncouragement("Donec id risus justo. Vivamus feugiat orci vel nunc fringilla, in vulputate sapien convallis. Sed volutpat risus lorem, nec lobortis massa mattis eget");
        ctlVerse.setToken("d54240fc6c96b2724ec77a1f99731bab0e5a81e5e02e3aef5f954e2c4544a0a8");
        ctlVerse.setImage(true);


        return ctlVerse;
    }

}
