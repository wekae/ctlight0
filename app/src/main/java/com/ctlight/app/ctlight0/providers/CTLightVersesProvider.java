package com.ctlight.app.ctlight0.providers;

import com.ctlight.app.ctlight0.providers.api.CTLVerseAPI;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;
import com.ctlight.app.ctlight0.utils.HttpUtils;

import java.util.ArrayList;

import io.reactivex.Observable;

public class CTLightVersesProvider {

    static CTLVerseAPI ctlVerseAPI;
    static ArrayList<CTLVerse> ctlVerses = new ArrayList<>();
    private static final String LOG_TAG = CTLightVersesProvider.class.getSimpleName();


    public static Observable<JSONArrayResponse<CTLVerse>> allVerses(){


        // Initialize Retrofit API
        ctlVerseAPI = HttpUtils.initRetrofit().create(CTLVerseAPI.class);

        // Asynchronous Approach Using An Observable
        Observable<JSONArrayResponse<CTLVerse>> responseObservable = ctlVerseAPI.loadVerses();
        return responseObservable;

    }


    public static Observable<JSONObjectResponse<CTLVerse>> getVerse(String id){

        // Initialize Retrofit API
        ctlVerseAPI = HttpUtils.initRetrofit().create(CTLVerseAPI.class);

        // Asynchronous Approach Using An Observable
        Observable<JSONObjectResponse<CTLVerse>> responseObservable = ctlVerseAPI.loadVerse(id);


        return responseObservable;

    }

    public static Observable<JSONObjectResponse<CTLVerse>> getTodaysVerse(){

        // Initialize Retrofit API
        ctlVerseAPI = HttpUtils.initRetrofit().create(CTLVerseAPI.class);

        // Asynchronous Approach Using An Observable
        Observable<JSONObjectResponse<CTLVerse>> responseObservable = ctlVerseAPI.loadTodaysVerse();


        return responseObservable;


    }







}
