package com.ctlight.app.ctlight0.providers.models;

import java.util.ArrayList;

public class CTLEvent {

    private Integer id;
    private String title;
    private String date;
    private String venue;
    private String description;
    private boolean image;
    private String token;
    private String additionalInfo;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getVenue() {
        return venue;
    }

    public String getDescription() {
        return description;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public boolean getImage() {
        return image;
    }

    public String getToken() {
        return token;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void setImage(boolean image) {
        this.image = image;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
