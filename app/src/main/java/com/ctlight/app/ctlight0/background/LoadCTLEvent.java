package com.ctlight.app.ctlight0.background;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ctlight.app.ctlight0.providers.CTLightEventsProvider;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;

public class LoadCTLEvent extends AsyncTask<String, Void, Observable<JSONObjectResponse<CTLEvent>>>{
    private WeakReference<Context> mContext;

    private static final String LOG_TAG = LoadCTLEvent.class.getSimpleName();

    public AsyncResponse<Observable<JSONObjectResponse<CTLEvent>>> delegate = null;

    public LoadCTLEvent(Context ctx){
        this.mContext = new WeakReference<>(ctx);
    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Toast.makeText(mContext.get(), "LOADING...", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPostExecute(Observable<JSONObjectResponse<CTLEvent>> responseObservable) {
        Log.d(LOG_TAG, "POST EXECUTE");
        delegate.processFinish(responseObservable);
    }

    @Override
    protected Observable<JSONObjectResponse<CTLEvent>> doInBackground(String... strings) {
        String id = strings[0];
        Observable<JSONObjectResponse<CTLEvent>> responseObservable = CTLightEventsProvider.getEvent(id);
        return responseObservable;
    }
}
