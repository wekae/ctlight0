package com.ctlight.app.ctlight0.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;

public class AnimationUtils {
    public static Animation fadeIn(){
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000);

        return fadeIn;
    }

    public static Animation fadeOut(){
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1000);

        return fadeOut;
    }

    public View setAnimationSet(View view){
        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeIn());
        animation.addAnimation(fadeOut());
        view.setAnimation(animation);

        return view;
    }
}
