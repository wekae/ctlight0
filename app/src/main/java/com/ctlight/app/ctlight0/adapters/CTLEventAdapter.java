package com.ctlight.app.ctlight0.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.ui.CTLEventDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLEventDetailFragment;
import com.ctlight.app.ctlight0.utils.AnimationUtils;
import com.ctlight.app.ctlight0.utils.HttpUtils;

import java.util.List;

public class CTLEventAdapter extends RecyclerView.Adapter<CTLEventAdapter.ViewHolder> {


    private final FragmentActivity mParentActivity;
    private Context context;
    private List<CTLEvent> mValues;
    private final boolean mTwoPane;

    private final LayoutInflater mInflater;

    private static final String LOG_TAG = CTLEventAdapter.class.getSimpleName();


    public CTLEventAdapter(Activity parent, boolean twoPane) {
        mParentActivity = (FragmentActivity)parent;
        context = parent;
        mTwoPane = twoPane;

        mInflater = LayoutInflater.from(context);
    }


    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            CTLEvent item = (CTLEvent) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(CTLEventDetailFragment.ARG_ITEM_ID, item.getId().toString());
                CTLEventDetailFragment fragment = new CTLEventDetailFragment();
                fragment.setArguments(arguments);

                // Call another fragment
                FragmentTransaction fragmentTransaction = mParentActivity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.ctlevent_detail_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, CTLEventDetailActivity.class);
                intent.putExtra(CTLEventDetailFragment.ARG_ITEM_ID, item.getId().toString());

                context.startActivity(intent);
            }
        }
    };

    @Override
    public CTLEventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.ctlevent_list_content, parent, false);
        return new CTLEventAdapter.ViewHolder(view);
    }

    public void setValues(List<CTLEvent> values){
        mValues = values;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final CTLEventAdapter.ViewHolder holder, int position) {
        Uri baseUri = Uri.parse(HttpUtils.BASE_URL);
        Uri imageUri = Uri.withAppendedPath(baseUri, "event/image/"+mValues.get(position).getToken());

//        Log.d(LOG_TAG, "IMAGE: "+mValues.get(position).getImage());
//        if(mValues.get(position).getImage()==null){
//
//
//            //Change relative position
//            RelativeLayout.LayoutParams txtTitleRelativeParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//            txtTitleRelativeParams.addRule(RelativeLayout.BELOW, holder.mImgEvent.getId());
//
//            holder.mTxtTitle.setLayoutParams(txtTitleRelativeParams);
//
//            RelativeLayout.LayoutParams txtVenueRelativeParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//            txtVenueRelativeParams.addRule(RelativeLayout.BELOW, holder.mTxtTitle.getId());
//
//            holder.mTxtVenue.setLayoutParams(txtVenueRelativeParams);
//        }

        holder.mTxtTitle.setText(mValues.get(position).getTitle());
        holder.mTxtVenue.setText(mValues.get(position).getVenue());
        holder.mTxtDate.setText(mValues.get(position).getDate());
        holder.mTxtDescription.setText(mValues.get(position).getDescription());
        Glide.with(context)
                .load(imageUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.mImgEvent);

        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtTitle;
        private TextView mTxtVenue;
        private TextView mTxtDate;
        private TextView mTxtDescription;
        private ImageView mImgEvent;

        ViewHolder(View view) {
            super(view);
            mTxtTitle = (TextView) view.findViewById(R.id.txt_card_title);
            mTxtVenue = (TextView) view.findViewById(R.id.txt_card_venue);
            mTxtDate = (TextView) view.findViewById(R.id.txt_card_date);
            mTxtDescription = (TextView) view.findViewById(R.id.txt_card_description);
            mImgEvent = (ImageView) view.findViewById(R.id.img_card_event);

            view.setAnimation(AnimationUtils.fadeIn());
        }
    }
}