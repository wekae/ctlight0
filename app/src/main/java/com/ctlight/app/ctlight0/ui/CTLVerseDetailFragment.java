package com.ctlight.app.ctlight0.ui;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ctlight.app.ctlight0.MainActivity;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.background.AsyncResponse;
import com.ctlight.app.ctlight0.background.LoadCTLTodaysVerse;
import com.ctlight.app.ctlight0.background.LoadCTLVerse;
import com.ctlight.app.ctlight0.providers.CTLightVersesProvider;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;
import com.ctlight.app.ctlight0.providers.viewmodels.CTLVerseViewModel;
import com.ctlight.app.ctlight0.utils.AnimationUtils;
import com.ctlight.app.ctlight0.utils.HttpUtils;
import com.ctlight.app.ctlight0.utils.NotificationUtils;

import java.util.Calendar;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A fragment representing a single CTLVerse detail screen.
 * in two-pane mode (on tablets) or a {@link CTLVerseDetailActivity}
 * on handsets.
 */
public class CTLVerseDetailFragment extends  Fragment implements AsyncResponse<Observable<JSONObjectResponse<CTLVerse>>> {


    public static final String ARG_ITEM_ID = "item_id";
    public static final String INSTANCE_KEY = "instance_key";
    private static final String LOG_TAG = CTLVerseDetailFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = CTLVerseDetailFragment.class.getSimpleName();

    public static final String KEY_ITEM = "unique_key";
    public static final String KEY_INDEX = "index_key";
    private String mTime;

    private Context context;
    private Activity activity;
    private View rootView;


    private LinearLayout lytLoading;


    private CTLVerseViewModel ctlVerseViewModel;

    /**
     * The content this fragment is presenting.
     */
    private CTLVerse mItem;

    private String id;
    private boolean todaysVerse;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CTLVerseDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity activity = this.getActivity();

        //Get today's verse
        if(getArguments().containsKey(MainActivity.TODAYS_VERSE)){
            if(getArguments().getBoolean(MainActivity.TODAYS_VERSE)){
                todaysVerse = true;
            }
        }else{
            //Get Selected Verse/ Verse Stored during reconfiguration


            if (getArguments().containsKey(ARG_ITEM_ID)) {
                // Load the content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                id = getArguments().getString(ARG_ITEM_ID);

            }else if(savedInstanceState!=null){
                id = savedInstanceState.get(INSTANCE_KEY).toString();
                // Restore last state
                mTime = savedInstanceState.getString("time_key");
            } else {
                mTime = "" + Calendar.getInstance().getTimeInMillis();
            }
        }
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if(mItem!=null){
            outState.putInt(INSTANCE_KEY, mItem.getId());
        }


        outState.putString("time_key", mTime);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.ctlverse_detail, container, false);

        //Run if network connection is established
        if(HttpUtils.connectionExists(activity)){

            initFragment();

        }else{
            NotificationUtils.showDialogBox(activity, "No Network", "Check your internet connection");
        }

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.collapsing_toolbar_layout);
        AppBarLayout appBarLayout = (AppBarLayout) activity.findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Verse Details");
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        context = ctx;
        activity = (Activity)ctx;
    }

    private void initFragment(){
//        if(todaysVerse){
//            LoadCTLTodaysVerse loadCTLTodaysVerse= new LoadCTLTodaysVerse(context);
//            loadCTLTodaysVerse.delegate = this;
//
//            //Implement asynctask
//            loadCTLTodaysVerse.execute();
//        }else{
//            LoadCTLVerse loadCTLVerse = new LoadCTLVerse(context);
//            loadCTLVerse.delegate = this;
//
//            //Implement asynctask
//            loadCTLVerse.execute(id);
//        }

/**
        ctlEventViewModel = ViewModelProviders.of(this).get(CTLEventViewModel.class);
        ctlEventViewModel.getCtlEvent(id).observe(this, new android.arch.lifecycle.Observer<JSONObjectResponse<CTLEvent>>() {
            @Override
            public void onChanged(@Nullable JSONObjectResponse<CTLEvent> ctlEventJSONArrayResponse) {

                mItem = ctlEventJSONArrayResponse.getData();
                Toast.makeText(context, "Finishing: "+mItem.getTitle(), Toast.LENGTH_LONG).show();
                populateFields();

            }
        });
 **/
        if(todaysVerse){
            ctlVerseViewModel = ViewModelProviders.of(this).get(CTLVerseViewModel.class);
            ctlVerseViewModel.getCtlTodaysVerse().observe(this, new android.arch.lifecycle.Observer<JSONObjectResponse<CTLVerse>>() {
                @Override
                public void onChanged(@Nullable JSONObjectResponse<CTLVerse> ctlVerseJSONArrayResponse) {

                    mItem = ctlVerseJSONArrayResponse.getData();
//                    Toast.makeText(context, "Finishing: "+mItem.getTitle(), Toast.LENGTH_LONG).show();
                    populateFields();

                }
            });
        }else{
            ctlVerseViewModel = ViewModelProviders.of(this).get(CTLVerseViewModel.class);
            ctlVerseViewModel.getCtlVerse(id).observe(this, new android.arch.lifecycle.Observer<JSONObjectResponse<CTLVerse>>() {
                @Override
                public void onChanged(@Nullable JSONObjectResponse<CTLVerse> ctlVerseJSONArrayResponse) {

                    mItem = ctlVerseJSONArrayResponse.getData();
//                    Toast.makeText(context, "Finishing: "+mItem.getTitle(), Toast.LENGTH_LONG).show();
                    populateFields();

                }
            });
        }

    }

    @Override
    public void processFinish(Observable<JSONObjectResponse<CTLVerse>> responseObservable){
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JSONObjectResponse<CTLVerse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(JSONObjectResponse<CTLVerse> value) {
                        mItem = value.getData();
//                        Toast.makeText(context, "Finishing: "+mItem.getTitle(), Toast.LENGTH_LONG).show();

                        // Show the Event content as text in respective TextViews.
                        // Show the Event content as text in respective TextViews.
                        if (mItem != null) {
                            ((TextView) rootView.findViewById(R.id.txt_verse_title)).setText(mItem.getTitle());
                            ((TextView) rootView.findViewById(R.id.txt_verse_date)).setText(mItem.getDate());
                            ((TextView) rootView.findViewById(R.id.lbl_verse_verse)).setText(mItem.getVerse());
                            ((TextView) rootView.findViewById(R.id.txt_verse_contents)).setText(mItem.getContents());
                            ((TextView) rootView.findViewById(R.id.txt_verse_description)).setText(mItem.getDescription());
                            ((TextView) rootView.findViewById(R.id.txt_verse_encouragement)).setText(mItem.getEncouragement());


                            if(mItem.getImage()){
                                Uri baseUri = Uri.parse(HttpUtils.BASE_URL);
                                Uri imageUri = Uri.withAppendedPath(baseUri, "verse/image/"+mItem.getToken());


                                if (activity.findViewById(R.id.ctlverse_list) != null) {
                                    // The detail container view will be present only in the
                                    // large-screen layouts (res/values-w900dp).
                                    // If this view is present, then the
                                    // activity should be in two-pane mode.
                                    Glide.with(context)
                                            .load(imageUri)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .placeholder(R.drawable.ct_light_banner_logo_2)
                                            .crossFade()
                                            .into(((ImageView) rootView.findViewById(R.id.img_verse)));
                                }else{
                                    // If not two pane
                                    Glide.with(context)
                                            .load(imageUri)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .placeholder(R.drawable.ct_light_banner_logo_2)
                                            .crossFade()
                                            .into(((ImageView) activity.findViewById(R.id.img_verse_detail)));

                                }
                            }

                        }

                        RelativeLayout layout = activity.findViewById(R.id.lyt_ctlverse_detail);
                        layout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    private void populateFields(){
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.txt_verse_title)).setText(mItem.getTitle());
            ((TextView) rootView.findViewById(R.id.txt_verse_date)).setText(mItem.getDate());
            ((TextView) rootView.findViewById(R.id.lbl_verse_verse)).setText(mItem.getVerse());
            ((TextView) rootView.findViewById(R.id.txt_verse_contents)).setText(mItem.getContents());
            ((TextView) rootView.findViewById(R.id.txt_verse_description)).setText(mItem.getDescription());
            ((TextView) rootView.findViewById(R.id.txt_verse_encouragement)).setText(mItem.getEncouragement());


            if(mItem.getImage()){
                Uri baseUri = Uri.parse(HttpUtils.BASE_URL);
                Uri imageUri = Uri.withAppendedPath(baseUri, "verse/image/"+mItem.getToken());


                if (activity.findViewById(R.id.ctlverse_list) != null) {
                    // The detail container view will be present only in the
                    // large-screen layouts (res/values-w900dp).
                    // If this view is present, then the
                    // activity should be in two-pane mode.
                    Glide.with(context)
                            .load(imageUri)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ct_light_banner_logo_2)
                            .crossFade()
                            .into(((ImageView) rootView.findViewById(R.id.img_verse)));
                }else{
                    // If not two pane
                    Glide.with(context)
                            .load(imageUri)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ct_light_banner_logo_2)
                            .crossFade()
                            .into(((ImageView) activity.findViewById(R.id.img_verse_detail)));

                }
            }


            lytLoading = activity.findViewById(R.id.lyt_loading);
            lytLoading.setVisibility(View.GONE);

            RelativeLayout layout = activity.findViewById(R.id.lyt_ctlverse_detail);
            layout.setAnimation(AnimationUtils.fadeIn());
            layout.setVisibility(View.VISIBLE);

        }
    }
}
