package com.ctlight.app.ctlight0.providers.models;

public class PushMessage {
    private String title;
    private String message;
    private String id;
    private String redirect;

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }
}
