package com.ctlight.app.ctlight0.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.ui.CTLEventListActivity;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLVerseListActivity;

import java.util.ArrayList;
import java.util.List;

import static com.ctlight.app.ctlight0.MainActivity.TODAYS_VERSE;

public class MainPageAdapter extends RecyclerView.Adapter<MainPageAdapter.ViewHolder> {
    private Context mContext;

    private List<MainPageItem> mainPageItems = loadMainPageItems();


    public MainPageAdapter(Context context){
        mContext = context;
    }

    @Override
    public MainPageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.main_page_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        MainPageItem item = mainPageItems.get(i);
        viewHolder.bindTo(item);
    }

    @Override
    public int getItemCount() {
        return mainPageItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView txtTitle;
        private ImageView imgMainPage;

        ViewHolder(View itemView){
            super(itemView);

            txtTitle = itemView.findViewById(R.id.txt_main_page_title);
            imgMainPage = itemView.findViewById(R.id.img_main_page_item);

            itemView.setOnClickListener(this);
        }

        void bindTo(MainPageItem item){
            txtTitle.setText(item.getName());
            Glide.with(mContext)
                    .load(item.getImageResource())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgMainPage);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            int id = mainPageItems.get(position).getId();

            if (id == R.id.nav_events) {

                Intent intent = new Intent(mContext, CTLEventListActivity.class);
                mContext.startActivity(intent);

            } else if (id == R.id.nav_all_verses) {

                Intent intent = new Intent(mContext, CTLVerseListActivity.class);
                mContext.startActivity(intent);

            } else if (id == R.id.nav_todays_verse) {

                // Handle the today's verse action
                Intent intent = new Intent(mContext, CTLVerseDetailActivity.class);
                intent.putExtra(TODAYS_VERSE, true);
                mContext.startActivity(intent);

            }
        }
    }
    class MainPageItem{
        int id;
        String name;
        String description;
        int imageResource;

        MainPageItem(int id, String name, String description, int imageResource){
            this.id = id;
            this.name = name;
            this.description = description;
            this.imageResource = imageResource;
        }

        public int getId() {
            return id;
        }

        public String getDescription() {
            return description;
        }

        public String getName() {
            return name;
        }

        public int getImageResource() {
            return imageResource;
        }
    }


    public List<MainPageItem> loadMainPageItems(){
        List<MainPageItem> items = new ArrayList<>();

        MainPageItem events = new MainPageItem(R.id.nav_events,"Events", null, R.drawable.ct_events);
        MainPageItem verses = new MainPageItem(R.id.nav_all_verses,"Verses", null, R.drawable.ct_verses);
        MainPageItem todaysVerse = new MainPageItem(R.id.nav_todays_verse,"Today's Verse", null, R.drawable.ct_todays_verse);

        items.add(events);
        items.add(verses);
        items.add(todaysVerse);

        return items;
    }
}
