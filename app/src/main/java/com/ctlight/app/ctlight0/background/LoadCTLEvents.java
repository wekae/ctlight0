package com.ctlight.app.ctlight0.background;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ctlight.app.ctlight0.providers.CTLightEventsProvider;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoadCTLEvents extends AsyncTask<Void, Void, Observable<JSONArrayResponse<CTLEvent>>>{
    private WeakReference<Context> mContext;

    private static final String LOG_TAG = LoadCTLEvents.class.getSimpleName();

    public AsyncResponse<Observable<JSONArrayResponse<CTLEvent>>> delegate = null;

    public LoadCTLEvents(Context ctx){
        this.mContext = new WeakReference<>(ctx);
    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Toast.makeText(mContext.get(), "LOADING...", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPostExecute(Observable<JSONArrayResponse<CTLEvent>> responseObservable) {
        Log.d(LOG_TAG, "POST EXECUTE");
        delegate.processFinish(responseObservable);
    }

    @Override
    protected Observable<JSONArrayResponse<CTLEvent>> doInBackground(Void... voids) {
        Observable<JSONArrayResponse<CTLEvent>> responseObservable = CTLightEventsProvider.allEvents();
        return responseObservable;
    }
}
