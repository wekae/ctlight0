package com.ctlight.app.ctlight0.providers.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.repository.CTLEventsRepository;

import java.util.ArrayList;

public class CTLEventsViewModel extends AndroidViewModel {
    private CTLEventsRepository ctlEventsRepository;


    private LiveData<JSONArrayResponse<CTLEvent>> ctlEvents;

    public CTLEventsViewModel(Application application){
        super(application);

        ctlEventsRepository = new CTLEventsRepository(application);
        ctlEvents = ctlEventsRepository.getAllEvents();
    }


    public LiveData<JSONArrayResponse<CTLEvent>> getCtlEvents() {
        return ctlEvents;
    }
}
