package com.ctlight.app.ctlight0.services;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ctlight.app.ctlight0.BuildConfig;
import com.ctlight.app.ctlight0.MainActivity;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.providers.models.PushMessage;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailFragment;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONException;
import org.json.JSONObject;

public class NewContentService extends IntentService {

    public static final String LOG_TAG = NewContentService.class.getSimpleName();

    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private NotificationManager mNotificationManager;
    private static final int NOTIFICATION_ID = 0;
    private static final String ACTION_UPDATE_NOTIFICATION = BuildConfig.APPLICATION_ID+"ACTION_UPDATE_NOTIFICATION";


    public NewContentService(){
        super("NewContentService");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "Service Started");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        initPusher();

    }

    private void initPusher(){
        PusherOptions options = new PusherOptions();
        options.setCluster("mt1");
        final Pusher pusher = new Pusher("2b5f116d10de99ed6de0", options);

//        Channel ctlEventsChannel = pusher.subscribe("ctlevent-channel");
//
//        ctlEventsChannel.bind("ctl-event", new SubscriptionEventListener() {
//            @Override
//            public void onEvent(String channelName, String eventName, final String data) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(activity, data, Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        });

        Channel ctlVersesChannel = pusher.subscribe("ctlverse-channel");

        ctlVersesChannel.bind("ctl-verse", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                try {
                    JSONObject message = new JSONObject(data);
                    PushMessage pushMessage = new PushMessage();
                    pushMessage.setRedirect("verse");
                    pushMessage.setId(message.getString("id"));
                    pushMessage.setTitle(message.getString("title"));
                    pushMessage.setMessage(message.getString("message"));

                    sendNotification(pushMessage);
                }catch (JSONException e){
                    Log.e(LOG_TAG, "FAILURE", e);
                }
            }
        });

        pusher.connect();
    }

    public void createNotificationChannel(){
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //Create a NotificationChannel
            NotificationChannel notificationChannel = new NotificationChannel(
                    PRIMARY_CHANNEL_ID,
                    "Mascot Notification",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notification from Mascot");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private NotificationCompat.Builder getNotificationBuilder(PushMessage pushMessage){
        Intent notificationIntent = null;
        if(pushMessage.getRedirect().equals("verse")){
            notificationIntent = new Intent(this, CTLVerseDetailActivity.class);
            notificationIntent.putExtra(CTLVerseDetailFragment.ARG_ITEM_ID, pushMessage.getId());
        }
        // Create content intent that is launched when the user clicks the notification
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setContentTitle(pushMessage.getTitle())
                .setContentText(pushMessage.getMessage())
                .setSmallIcon(R.drawable.cl_light_logo)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        return notificationBuilder;

    }

    public void sendNotification(PushMessage pushMessage){
        Intent updateIntent = new Intent(ACTION_UPDATE_NOTIFICATION);
        PendingIntent updatePendingIntent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, updateIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = getNotificationBuilder(pushMessage);

        mNotificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

    }
}
