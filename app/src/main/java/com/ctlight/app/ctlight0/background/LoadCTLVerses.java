package com.ctlight.app.ctlight0.background;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ctlight.app.ctlight0.providers.CTLightEventsProvider;
import com.ctlight.app.ctlight0.providers.CTLightVersesProvider;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;

public class LoadCTLVerses extends AsyncTask<Void, Void, Observable<JSONArrayResponse<CTLVerse>>>{
    private WeakReference<Context> mContext;

    private static final String LOG_TAG = LoadCTLVerses.class.getSimpleName();

    public AsyncResponse<Observable<JSONArrayResponse<CTLVerse>>> delegate = null;

    public LoadCTLVerses(Context ctx){
        this.mContext = new WeakReference<>(ctx);
    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Toast.makeText(mContext.get(), "LOADING...", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPostExecute(Observable<JSONArrayResponse<CTLVerse>> responseObservable) {
        Log.d(LOG_TAG, "POST EXECUTE");
        delegate.processFinish(responseObservable);
    }

    @Override
    protected Observable<JSONArrayResponse<CTLVerse>> doInBackground(Void... voids) {
        Observable<JSONArrayResponse<CTLVerse>> responseObservable = CTLightVersesProvider.allVerses();
        return responseObservable;
    }
}
