package com.ctlight.app.ctlight0.ui;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.adapters.CTLEventAdapter;
import com.ctlight.app.ctlight0.background.AsyncResponse;
import com.ctlight.app.ctlight0.background.LoadCTLEvent;
import com.ctlight.app.ctlight0.background.LoadCTLEvents;
import com.ctlight.app.ctlight0.providers.CTLightEventsProvider;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;
import com.ctlight.app.ctlight0.providers.viewmodels.CTLEventViewModel;
import com.ctlight.app.ctlight0.utils.AnimationUtils;
import com.ctlight.app.ctlight0.utils.HttpUtils;
import com.ctlight.app.ctlight0.utils.NotificationUtils;

import java.util.ArrayList;
import java.util.Calendar;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A fragment representing a single CTLEvent detail screen.
 * in two-pane mode (on tablets) or a {@link CTLEventDetailActivity}
 * on handsets.
 */
public class CTLEventDetailFragment extends Fragment implements AsyncResponse<Observable<JSONObjectResponse<CTLEvent>>>{


    public static final String ARG_ITEM_ID = "item_id";
    public static final String INSTANCE_KEY = "instance_key";
    private static final String LOG_TAG = CTLEventDetailFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = CTLEventDetailFragment.class.getSimpleName();

    public static final String KEY_ITEM = "unique_key";
    public static final String KEY_INDEX = "index_key";
    private String mTime;

    private Context context;
    private Activity activity;
    private View rootView;


    private LinearLayout lytLoading;



    private CTLEventViewModel ctlEventViewModel;

    /**
     * The content this fragment is presenting.
     */
    private CTLEvent mItem;

    private String id;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CTLEventDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            id = getArguments().getString(ARG_ITEM_ID);

        }else if(savedInstanceState!=null){
            id = savedInstanceState.get(INSTANCE_KEY).toString();
        }

        if (savedInstanceState != null) {
            // Restore last state
            mTime = savedInstanceState.getString("time_key");
        } else {
            mTime = "" + Calendar.getInstance().getTimeInMillis();
        }
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if(mItem!=null){
            outState.putInt(INSTANCE_KEY, mItem.getId());
        }


        outState.putString("time_key", mTime);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.ctlevent_detail, container, false);


        //Run if network connection is established
        if(HttpUtils.connectionExists(activity)){

            initFragment();

        }else{
            NotificationUtils.showDialogBox(activity, "No Network", "Check your internet connection");
        }



        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.collapsing_toolbar_layout);
        AppBarLayout appBarLayout = (AppBarLayout) activity.findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Event Details");
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        context = ctx;
        activity = (Activity) context;
    }



    private void initFragment(){
//        LoadCTLEvent loadCTLEvent = new LoadCTLEvent(context);
//        loadCTLEvent.delegate = this;
//
//        //Implement asynctask
//        loadCTLEvent.execute(id);

        ctlEventViewModel = ViewModelProviders.of(this).get(CTLEventViewModel.class);
        ctlEventViewModel.getCtlEvent(id).observe(this, new android.arch.lifecycle.Observer<JSONObjectResponse<CTLEvent>>() {
            @Override
            public void onChanged(@Nullable JSONObjectResponse<CTLEvent> ctlEventJSONArrayResponse) {

                mItem = ctlEventJSONArrayResponse.getData();
//                Toast.makeText(context, "Finishing: "+mItem.getTitle(), Toast.LENGTH_LONG).show();
                populateFields();

            }
        });
    }



    //Used With AsyncTask
    @Override
    public void processFinish(Observable<JSONObjectResponse<CTLEvent>> responseObservable){
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JSONObjectResponse<CTLEvent>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(JSONObjectResponse<CTLEvent> value) {
                        mItem = value.getData();
//                        Toast.makeText(context, "Finishing: "+mItem.getTitle(), Toast.LENGTH_LONG).show();
                        populateFields();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    private void populateFields(){
        // Show the Event content as text in respective TextViews.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.txt_event_title)).setText(mItem.getTitle());
            ((TextView) rootView.findViewById(R.id.txt_event_venue)).setText(mItem.getVenue());
            ((TextView) rootView.findViewById(R.id.txt_event_date)).setText(mItem.getDate());
            ((TextView) rootView.findViewById(R.id.txt_event_description)).setText(mItem.getDescription());
            ((TextView) rootView.findViewById(R.id.txt_event_additional_info)).setText(mItem.getAdditionalInfo());

            if(mItem.getImage()){
                Uri baseUri = Uri.parse(HttpUtils.BASE_URL);
                Uri imageUri = Uri.withAppendedPath(baseUri, "event/image/"+mItem.getToken());


                if (activity.findViewById(R.id.ctlevent_list) != null) {
                    // The detail container view will be present only in the
                    // large-screen layouts (res/values-w900dp).
                    // If this view is present, then the
                    // activity should be in two-pane mode.
                    Glide.with(context)
                            .load(imageUri)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ct_light_banner_logo_2)
                            .crossFade()
                            .into(((ImageView) rootView.findViewById(R.id.img_event)));
                }else{
                    // If not two pane
                    Glide.with(context)
                            .load(imageUri)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ct_light_banner_logo_2)
                            .crossFade()
                            .into(((ImageView) activity.findViewById(R.id.img_event_detail)));

                }
            }


            lytLoading = activity.findViewById(R.id.lyt_loading);
            lytLoading.setVisibility(View.GONE);

            RelativeLayout layout = activity.findViewById(R.id.lyt_ctlevent_detail);
            layout.setAnimation(AnimationUtils.fadeIn());
            layout.setVisibility(View.VISIBLE);

        }
    }
}
