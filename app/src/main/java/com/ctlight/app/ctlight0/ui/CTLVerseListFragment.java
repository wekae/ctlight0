package com.ctlight.app.ctlight0.ui;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.adapters.CTLVerseAdapter;
import com.ctlight.app.ctlight0.background.AsyncResponse;
import com.ctlight.app.ctlight0.background.LoadCTLVerses;
import com.ctlight.app.ctlight0.providers.CTLightVersesProvider;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.viewmodels.CTLVersesViewModel;
import com.ctlight.app.ctlight0.utils.HttpUtils;
import com.ctlight.app.ctlight0.utils.NotificationUtils;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class CTLVerseListFragment extends Fragment  implements AsyncResponse<Observable<JSONArrayResponse<CTLVerse>>> {

    public static final String LOG_TAG = CTLVerseListFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = CTLVerseListFragment.class.getSimpleName();


    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private Context context;
    private Activity activity;

    private RecyclerView mRecyclerView;

    static CTLVerseListFragment fragment;

    private CTLVersesViewModel ctlVersesViewModel;


    private LinearLayout lytLoading;



    public static final String KEY_ITEM = "unique_key";
    public static final String KEY_INDEX = "index_key";
    private String mTime;

    public CTLVerseListFragment() {
        // Required empty public constructor
    }

    public static CTLVerseListFragment newInstance() {
        if(fragment==null){
            fragment = new CTLVerseListFragment();
        }
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(LOG_TAG, "Creating Fragment");

        if (getArguments() != null) {

        }

        if (activity.findViewById(R.id.ctlverse_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ctlverse_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d(LOG_TAG, "Creating RecyclerView");
        mRecyclerView = activity.findViewById(R.id.ctlverse_list);
        assert mRecyclerView != null;
        setupRecyclerView();
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        context = ctx;
        activity = (Activity)ctx;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("time_key", mTime);
    }

    private void setupRecyclerView() {
//        LoadCTLVerses loadCTLVerses = new LoadCTLVerses(context);
//        loadCTLVerses.delegate = this;
//
//        //Implement asynctask
//        loadCTLVerses.execute();
        final CTLVerseAdapter ctlVerseAdapter = new CTLVerseAdapter(activity, mTwoPane);

        //Run if network connection is established
        if(HttpUtils.connectionExists(activity)){
            ctlVersesViewModel = ViewModelProviders.of(this).get(CTLVersesViewModel.class);
            ctlVersesViewModel.getCtlVerses().observe(this, new android.arch.lifecycle.Observer<JSONArrayResponse<CTLVerse>>() {
                @Override
                public void onChanged(@Nullable JSONArrayResponse<CTLVerse> ctlVerseJSONArrayResponse) {

                    ArrayList<CTLVerse> ctlVerses = new ArrayList<>();
                    ctlVerses = ctlVerseJSONArrayResponse.getData();
//                Toast.makeText(context, "Finishing: "+ctlVerses.size(), Toast.LENGTH_LONG).show();
//                for(CTLVerse verse: ctlVerses){
//                    Log.d(LOG_TAG, "VERSE: "+verse.getTitle());
//                }

                    ctlVerseAdapter.setValues(ctlVerses);

                    mRecyclerView.setAdapter(ctlVerseAdapter);
                    mRecyclerView.setVisibility(View.VISIBLE);

                    lytLoading = activity.findViewById(R.id.lyt_loading);
                    lytLoading.setVisibility(View.GONE);


                    ctlVerseAdapter.notifyDataSetChanged();
                }
            });

        }else{
            NotificationUtils.showDialogBox(activity, "No Network", "Check your internet connection");
        }

    }

    //Used With AsyncTask
    @Override
    public void processFinish(Observable<JSONArrayResponse<CTLVerse>> responseObservable){
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JSONArrayResponse<CTLVerse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(JSONArrayResponse<CTLVerse> value) {
                        ArrayList<CTLVerse> ctlEvents = new ArrayList<>();
                        ctlEvents = value.getData();
//                        Toast.makeText(context, "Finishing: "+ctlEvents.size(), Toast.LENGTH_LONG).show();

                        mRecyclerView.setAdapter(new CTLVerseAdapter(activity, mTwoPane));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

}
