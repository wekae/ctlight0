package com.ctlight.app.ctlight0.background;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ctlight.app.ctlight0.providers.CTLightVersesProvider;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;

public class LoadCTLTodaysVerse extends AsyncTask<Void, Void, Observable<JSONObjectResponse<CTLVerse>>>{
    private WeakReference<Context> mContext;

    private static final String LOG_TAG = LoadCTLTodaysVerse.class.getSimpleName();

    public AsyncResponse<Observable<JSONObjectResponse<CTLVerse>>> delegate = null;

    public LoadCTLTodaysVerse(Context ctx){
        this.mContext = new WeakReference<>(ctx);
    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Toast.makeText(mContext.get(), "LOADING...", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPostExecute(Observable<JSONObjectResponse<CTLVerse>> responseObservable) {
        Log.d(LOG_TAG, "POST EXECUTE");
        delegate.processFinish(responseObservable);
    }

    @Override
    protected Observable<JSONObjectResponse<CTLVerse>> doInBackground(Void... voids) {
        Observable<JSONObjectResponse<CTLVerse>> responseObservable = CTLightVersesProvider.getTodaysVerse();
        return responseObservable;
    }
}
