package com.ctlight.app.ctlight0.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.utils.CTLightBaseActivity;

public class CTLEventListActivity extends CTLightBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ctlevent_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

         // Replace whatever is in the fragment_container view with this fragment,
         // and add the transaction to the back stack
//         CTLEventListFragment ctlEventListFragment = CTLEventListFragment.newInstance();
//         transaction.add(R.id.fragments_container,ctlEventListFragment);
//         // Commit the transaction
//         transaction.commit();




        if (savedInstanceState == null) {
            CTLEventListFragment ctlEventListFragment = CTLEventListFragment.newInstance();
            ctlEventListFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, ctlEventListFragment, CTLEventListFragment.FRAGMENT_TAG).commit();
        } else {
            CTLEventListFragment ctlEventListFragment = (CTLEventListFragment) getSupportFragmentManager().findFragmentByTag(CTLEventListFragment.FRAGMENT_TAG);
        }
    }

}
