package com.ctlight.app.ctlight0.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.ctlight.app.ctlight0.MainActivity;
import com.ctlight.app.ctlight0.R;
import com.ctlight.app.ctlight0.utils.CTLightBaseActivity;

/**
 * An activity representing a single CTLVerse detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link
 */
public class CTLVerseDetailActivity extends CTLightBaseActivity {

    private final String LOG_TAG = CTLVerseDetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ctlverse_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //


        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();

            //Handle for today's verse
            if(getIntent().getBooleanExtra(MainActivity.TODAYS_VERSE, false)){
                arguments.putBoolean(MainActivity.TODAYS_VERSE, true);
            }


            //Handle for verses selected in the list
            if(getIntent().hasExtra(CTLVerseDetailFragment.ARG_ITEM_ID)){
                arguments.putString(CTLVerseDetailFragment.ARG_ITEM_ID,
                        getIntent().getStringExtra(CTLVerseDetailFragment.ARG_ITEM_ID));

                Log.d(LOG_TAG, "ITEM ID: "+getIntent().getStringExtra(CTLVerseDetailFragment.ARG_ITEM_ID));
            }

            arguments.putAll(getIntent().getExtras());


            CTLVerseDetailFragment fragment = new CTLVerseDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().replace(R.id.ctlverse_detail_container, fragment, CTLVerseDetailFragment.FRAGMENT_TAG).commit();
        }else {
            CTLVerseDetailFragment ctlVerseDetailFragment = (CTLVerseDetailFragment) getSupportFragmentManager().findFragmentByTag(CTLVerseDetailFragment.FRAGMENT_TAG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, CTLVerseListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
