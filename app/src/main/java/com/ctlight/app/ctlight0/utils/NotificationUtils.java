package com.ctlight.app.ctlight0.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class NotificationUtils {


    public static void showToast(Activity activity, String message){
        WeakReference<Activity> activityWeakReference = new WeakReference<>(activity);
        final WeakReference<Context> contextWeakReference = new WeakReference<>((Context) activityWeakReference.get());

        final String msg = message;
        activityWeakReference.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(contextWeakReference.get(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void showSnackbar(Activity activity, String message){

        final WeakReference<Activity> activityWeakReference = new WeakReference<>(activity);
        final WeakReference<Context> contextWeakReference = new WeakReference<>((Context) activityWeakReference.get());

        final String msg = message;
        activityWeakReference.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(activityWeakReference.get().getCurrentFocus(), msg, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public static void showDialogBox(Activity activity, String title, String message){
        WeakReference<Activity> activityWeakReference = new WeakReference<>(activity);

        final String msg  = message;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activityWeakReference.get());

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                    }
                });
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        //Cancel Logic
//
//                    }
//                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
