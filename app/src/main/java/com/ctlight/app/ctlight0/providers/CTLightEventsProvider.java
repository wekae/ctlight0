package com.ctlight.app.ctlight0.providers;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.ctlight.app.ctlight0.providers.api.CTLEventAPI;
import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;
import com.ctlight.app.ctlight0.utils.HttpUtils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CTLightEventsProvider {

    static CTLEventAPI ctlEventAPI;
    static ArrayList<CTLEvent> ctlEvents = new ArrayList<>();
    private static final String LOG_TAG = CTLightEventsProvider.class.getSimpleName();



    public static Observable<JSONArrayResponse<CTLEvent>> allEvents(){


        // Initialize Retrofit API
        ctlEventAPI = HttpUtils.initRetrofit().create(CTLEventAPI.class);

        // Asynchronous Approach Using An Observable
        Observable<JSONArrayResponse<CTLEvent>> responseObservable = ctlEventAPI.loadEvents();
        return responseObservable;

    }

    public static Observable<JSONObjectResponse<CTLEvent>> getEvent(String id){

        // Initialize Retrofit API
        ctlEventAPI = HttpUtils.initRetrofit().create(CTLEventAPI.class);

        // Asynchronous Approach Using An Observable
        Observable<JSONObjectResponse<CTLEvent>> responseObservable = ctlEventAPI.loadEvent(id);


        return responseObservable;

    }



    //Asynchronous Approach Using A Callback
    static Callback<JSONArrayResponse<CTLEvent>> eventsCallback = new Callback<JSONArrayResponse<CTLEvent>>() {
        @Override
        public void onResponse(Call<JSONArrayResponse<CTLEvent>> call, Response<JSONArrayResponse<CTLEvent>> response) {
            if(response.isSuccessful()){
                Log.d(LOG_TAG, "RESPONSE");
                ctlEvents = response.body().getData();
                ctlEvents.size();
            } else {
                Log.d(LOG_TAG, "Code: " + response.code() + " Message: " + response.message());
            }
        }

        @Override
        public void onFailure(Call<JSONArrayResponse<CTLEvent>> call, Throwable t) {
            Log.e(LOG_TAG, "FAILURE : ", t);
        }
    };


}
