package com.ctlight.app.ctlight0.providers.api;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CTLVerseAPI {
    @GET("verse/")
    Observable<JSONArrayResponse<CTLVerse>> loadVerses();

    @GET("verse/{id}")
    Observable<JSONObjectResponse<CTLVerse>> loadVerse(@Path("id") String id);

    @GET("verse/today")
    Observable<JSONObjectResponse<CTLVerse>> loadTodaysVerse();
 }
