package com.ctlight.app.ctlight0.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ctlight.app.ctlight0.BuildConfig;
import com.ctlight.app.ctlight0.ui.CTLEventDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLEventDetailFragment;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailActivity;
import com.ctlight.app.ctlight0.ui.CTLVerseDetailFragment;
import com.google.firebase.messaging.RemoteMessage;
import com.pusher.pushnotifications.PushNotificationReceivedListener;
import com.pusher.pushnotifications.PushNotifications;

import java.util.HashSet;
import java.util.Set;

public class CTLightBaseActivity extends AppCompatActivity {

    public static final String CTL_EVENT_INTEREST = "ctlevent";
    public static final String CTL_VERSE_INTEREST = "ctlverse";

    private static final int NEW_CONTENT_JOB_ID = 1000;
    private Intent mServiceIntent;

    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private NotificationManager mNotificationManager;
    private static final int NOTIFICATION_ID = 0;
    private static final String ACTION_UPDATE_NOTIFICATION = BuildConfig.APPLICATION_ID+"ACTION_UPDATE_NOTIFICATION";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PushNotifications.start(getApplicationContext(), "d9dfca93-bc88-4928-9ffa-220f99dabba7");
        Set<String> subScriptionInterests = new HashSet<>();
        subScriptionInterests.add(CTL_EVENT_INTEREST);
        subScriptionInterests.add(CTL_VERSE_INTEREST);
        PushNotifications.setSubscriptions(subScriptionInterests);





        String redirect = getIntent().getStringExtra("redirect");
        String id = getIntent().getStringExtra("id");
        if(redirect!=null && id!=null){
            viewDetails(redirect, id);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        PushNotifications.setOnMessageReceivedListenerForVisibleActivity(this, new PushNotificationReceivedListener() {
            @Override
            public void onMessageReceived(RemoteMessage remoteMessage) {


                String redirect = remoteMessage.getData().get("redirect");
                String id = remoteMessage.getData().get("id");
                if(redirect!=null && id!=null){
                    notifyDetails(redirect, id);
                }
            }
        });
    }

    private void viewDetails(String redirect, String id){
        Intent intent = null;
        switch(redirect){
            case CTL_EVENT_INTEREST:
                intent = new Intent(this, CTLEventDetailActivity.class);
                intent.putExtra(CTLEventDetailFragment.ARG_ITEM_ID, id);
                startActivity(intent);
                break;

            case CTL_VERSE_INTEREST:
                intent = new Intent(this, CTLVerseDetailActivity.class);
                intent.putExtra(CTLVerseDetailFragment.ARG_ITEM_ID, id);
                startActivity(intent);
                break;

            default:
                break;
        }
    }

    private void notifyDetails(String redirect, String id){
        Intent intent = null;
        final Activity activity = this;
        switch(redirect){
            case CTL_EVENT_INTEREST:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "New Event Posted", Toast.LENGTH_LONG).show();
                    }
                });
//                intent = new Intent(this, CTLEventDetailActivity.class);
//                intent.putExtra(CTLEventDetailFragment.ARG_ITEM_ID, id);
//                startActivity(intent);
                break;

            case CTL_VERSE_INTEREST:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "New Verse Posted", Toast.LENGTH_LONG).show();
                    }
                });
//                intent = new Intent(this, CTLVerseDetailActivity.class);
//                intent.putExtra(CTLVerseDetailFragment.ARG_ITEM_ID, id);
//                startActivity(intent);
                break;

            default:
                break;
        }
    }

    //
//    private void initPusher(){
//        final Activity activity = this;
//        PusherOptions options = new PusherOptions();
//        options.setCluster("mt1");
//        final Pusher pusher = new Pusher("2b5f116d10de99ed6de0", options);
//
////        Channel ctlEventsChannel = pusher.subscribe("ctlevent-channel");
////
////        ctlEventsChannel.bind("ctl-event", new SubscriptionEventListener() {
////            @Override
////            public void onEvent(String channelName, String eventName, final String data) {
////                activity.runOnUiThread(new Runnable() {
////                    @Override
////                    public void run() {
////                        Toast.makeText(activity, data, Toast.LENGTH_LONG).show();
////                    }
////                });
////            }
////        });
//
//        Channel ctlVersesChannel = pusher.subscribe("ctlverse-channel");
//
//        ctlVersesChannel.bind("ctl-verse", new SubscriptionEventListener() {
//            @Override
//            public void onEvent(String channelName, String eventName, final String data) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            JSONObject message = new JSONObject(data);
//                            PushMessage pushMessage = new PushMessage();
//                            pushMessage.setRedirect("verse");
//                            pushMessage.setId(message.getString("id"));
//                            pushMessage.setTitle(message.getString("title"));
//                            pushMessage.setMessage(message.getString("message"));
//
//                            sendNotification(pushMessage);
//                        }catch (JSONException e){
//                            Log.e(LOG_TAG, "FAILURE", e);
//                        }
//                    }
//                });
//            }
//        });
//
//        pusher.connect();
//    }



//    public void createNotificationChannel(){
//        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//            //Create a NotificationChannel
//            NotificationChannel notificationChannel = new NotificationChannel(
//                    PRIMARY_CHANNEL_ID,
//                    "Mascot Notification",
//                    NotificationManager.IMPORTANCE_HIGH);
//            notificationChannel.enableLights(true);
//            notificationChannel.setLightColor(Color.RED);
//            notificationChannel.enableVibration(true);
//            notificationChannel.setDescription("Notification from Mascot");
//            mNotificationManager.createNotificationChannel(notificationChannel);
//        }
//    }
//
//    private NotificationCompat.Builder getNotificationBuilder(PushMessage pushMessage){
//        Intent notificationIntent = null;
//        if(pushMessage.getRedirect().equals("verse")){
//            notificationIntent = new Intent(this, CTLVerseDetailActivity.class);
//            notificationIntent.putExtra(CTLVerseDetailFragment.ARG_ITEM_ID, pushMessage.getId());
//        }
//        // Create content intent that is launched when the user clicks the notification
//        PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
//                .setContentTitle(pushMessage.getTitle())
//                .setContentText(pushMessage.getMessage())
//                .setSmallIcon(R.drawable.cl_light_logo)
//                .setContentIntent(notificationPendingIntent)
//                .setAutoCancel(true)
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setDefaults(NotificationCompat.DEFAULT_ALL);
//
//        return notificationBuilder;
//
//    }
//
//    public void sendNotification(PushMessage pushMessage){
//        Intent updateIntent = new Intent(ACTION_UPDATE_NOTIFICATION);
//        PendingIntent updatePendingIntent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, updateIntent, PendingIntent.FLAG_ONE_SHOT);
//
//        NotificationCompat.Builder notificationBuilder = getNotificationBuilder(pushMessage);
//
//        mNotificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
//
//    }
}
