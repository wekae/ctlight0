package com.ctlight.app.ctlight0.providers.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.widget.Toast;


import com.ctlight.app.ctlight0.providers.CTLightVersesProvider;
import com.ctlight.app.ctlight0.providers.models.CTLVerse;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CTLVersesRepository {
    private LiveData<io.reactivex.Observable<JSONArrayResponse<CTLVerse>>>  mCTLVerses;
    Application mApplication;
    public CTLVersesRepository(Application application){
        mApplication = application;
    }

    public LiveData<JSONArrayResponse<CTLVerse>> getAllVerses(){
        final MutableLiveData<JSONArrayResponse<CTLVerse>> data = new MutableLiveData<>();
        CTLightVersesProvider.allVerses().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JSONArrayResponse<CTLVerse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(JSONArrayResponse<CTLVerse> value) {
                        data.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
//                        Toast.makeText(mApplication, "DONE", Toast.LENGTH_SHORT).show();

                    }
                });

        return data;
    }

    public LiveData<JSONObjectResponse<CTLVerse>> getVerse(String id){
        final MutableLiveData<JSONObjectResponse<CTLVerse>> data = new MutableLiveData<>();
        CTLightVersesProvider.getVerse(id).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<JSONObjectResponse<CTLVerse>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(JSONObjectResponse<CTLVerse> value) {
                    data.setValue(value);
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {
//                    Toast.makeText(mApplication, "DONE", Toast.LENGTH_SHORT).show();

                }
        });

        return data;
    }

    public LiveData<JSONObjectResponse<CTLVerse>> getTodaysVerse(){
        final MutableLiveData<JSONObjectResponse<CTLVerse>> data = new MutableLiveData<>();
        CTLightVersesProvider.getTodaysVerse().subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<JSONObjectResponse<CTLVerse>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(JSONObjectResponse<CTLVerse> value) {
                    data.setValue(value);
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {
//                    Toast.makeText(mApplication, "DONE", Toast.LENGTH_SHORT).show();

                }
        });

        return data;
    }


}
