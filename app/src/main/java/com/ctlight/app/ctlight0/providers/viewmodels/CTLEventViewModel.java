package com.ctlight.app.ctlight0.providers.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;
import com.ctlight.app.ctlight0.providers.repository.CTLEventsRepository;

public class CTLEventViewModel extends AndroidViewModel {
    private CTLEventsRepository ctlEventsRepository;


    private LiveData<JSONObjectResponse<CTLEvent>> ctlEvent;
    private String mId;

    public CTLEventViewModel(Application application){
        super(application);

        ctlEventsRepository = new CTLEventsRepository(application);
    }


    public LiveData<JSONObjectResponse<CTLEvent>> getCtlEvent(String id) {
        if(ctlEvent!=null && mId!=null && mId.equals(id)) {
            return ctlEvent;
        }else{
            mId = id;
            ctlEvent = ctlEventsRepository.getEvent(id);
            return ctlEvent;
        }
    }
}
