package com.ctlight.app.ctlight0.providers.api;

import com.ctlight.app.ctlight0.providers.models.CTLEvent;
import com.ctlight.app.ctlight0.providers.models.JSONArrayResponse;
import com.ctlight.app.ctlight0.providers.models.JSONObjectResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CTLEventAPI {
    @GET("event/")
    Observable<JSONArrayResponse<CTLEvent>> loadEvents();

    @GET("event/{id}")
    Observable<JSONObjectResponse<CTLEvent>> loadEvent(@Path("id") String id);
 }
